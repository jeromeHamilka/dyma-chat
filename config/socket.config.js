const socketio = require("socket.io");
const { ensureAuthenticatedOnSocketHandshake } = require("./security.config");
const {
  createMessageController,
} = require("../controllers/message.controller");
const {
  tryGetRoomsController,
  createRoomController,
  updateRoomController,
  deleteRoomController,
  joinRoomController,
} = require("../controllers/room.controller");
let ios;
let activeUsers = [];

const emitActiveUsers = () => {
  const activeUsernames = activeUsers.map((user) => user.username).sort();
  ios.emit("activeUsers", activeUsernames);
};

const initSocketServer = (server) => {
  setInterval(() => {
    emitActiveUsers();
  }, 5000);

  ios = socketio(server, {
    allowRequest: ensureAuthenticatedOnSocketHandshake,
  });

  ios.on("connect", async (socket) => {
    const { userId: id, firstName: username } = socket.request.user;
    if (!activeUsers.find((user) => user.id === id)) {
      activeUsers.push({ id, username });
      emitActiveUsers();
    }

    tryGetRoomsController(socket);

    socket.on("getRooms", () => tryGetRoomsController(socket));
    socket.on("joinRoom", (roomId) => joinRoomController({ roomId, socket }));
    socket.on("leaveRoom", (roomId) => socket.leave(`/${roomId}`));

    socket.on("createRoom", (room) => createRoomController({ room, ios }));
    socket.on("updateRoom", (room) => updateRoomController({ room, ios }));
    socket.on("deleteRoom", (room) => deleteRoomController({ room, ios }));

    socket.on("message", (message) =>
      createMessageController({ ...message, ios, socket })
    );

    socket.on("isTyping", ({ roomId }) => {
      const { firstName } = socket.request.user;
      socket.to(`/${roomId}`).emit("typingUser", firstName);
    });

    socket.on("disconnect", () => {
      const { userId: id } = socket.request.user;
      activeUsers = activeUsers.filter((user) => user.id !== id);
    });
  });
};

module.exports = { initSocketServer };
