function createMessageItem(message) {
  message.time = new Date(message.updatedAt).toLocaleTimeString();
  const li = document.createElement("li");
  li.classList.add("item-message", "d-flex", "flex-row", "mb-2");
  li.innerHTML = `
    <span class="mr-1">${message.time}</span>
    <strong class="mr-3">${message.authorName}</strong>
    <span class="flex-fill">${message.data}</span>
  `;
  return li;
}

function displayMessages() {
  const messagesContainer = document.querySelector(".list-messages");
  const items = messages.map((message) => createMessageItem(message));
  messagesContainer.innerHTML = "";
  messagesContainer.prepend(...items);
  if (items.length) {
    items[items.length - 1].scrollIntoView();
  }
}

function displayNewMessage(message) {
  const messagesContainer = document.querySelector(".list-messages");
  const newMessage = createMessageItem(message);
  messagesContainer.append(newMessage);
  newMessage.scrollIntoView();
}

function displayTypingUsers(users) {
  const typingContainer = document.querySelector(".typing-info");
  let typingMessage = "";

  typingContainer.innerHTML = "";

  if (users.length) {
    const lastIndex = users.length - 1;

    typingMessage += `<img src="/images/typing.svg" />`;

    users.forEach((user, index) => {
      if (
        (users.length > 2 && index % users.length === 1) ||
        (index === 0 && users.length === 2)
      ) {
        typingMessage += `<strong>${user}</strong> et `;
      } else if (index < 4) {
        typingMessage += `<strong>${user}</strong>${
          index === lastIndex ? " " : ", "
        }`;
      } else if (index > 3) {
        typingMessage += `et d'autres `;
        return;
      }
    });
    typingMessage += `${
      users.length > 1 ? "sont" : "est"
    } en train d'écrire...`;
  }

  typingContainer.innerHTML = typingMessage;
}

function createUserItem(user) {
  const li = document.createElement("li");
  li.classList.add("item-user", "mb-1");
  li.innerHTML = ` ${user}`;
  return li;
}

function displayActiveUsers(users) {
  const usersContainer = document.querySelector(".list-users");
  const items = users.map((user) => createUserItem(user));
  usersContainer.innerHTML = "";
  usersContainer.prepend(...items);
}
